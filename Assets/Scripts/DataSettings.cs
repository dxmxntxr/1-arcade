﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Settings Data", menuName = "Settings")]
public class DataSettings : ScriptableObject
{
    [SerializeField] private int nowLevel;

    public int NowLevel
    {
        get => nowLevel;
        set => nowLevel = value;
    }
}
