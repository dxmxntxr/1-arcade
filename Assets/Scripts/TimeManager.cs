﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    private int _nowLeftSeconds = 1;

    [SerializeField] private VisualManager vManager;
    [SerializeField] private GameLogic logicManager;
    
    private static int MinutesLeft => _instance._nowLeftSeconds / 60;
    private static int SecondsWithLeft => _instance._nowLeftSeconds - MinutesLeft * 60;

    private static TimeManager _instance;

    private bool _canTimeNow = false;
        
    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        StartCoroutine(Timing());
    }

    public static void StartTimer(int seconds)
    {
        _instance._canTimeNow = true;
            
        _instance._nowLeftSeconds = seconds + 1;
    }

    public static void StopTimer()
    {
        _instance._canTimeNow = false;
    }

    private bool islosed = false;
    
    private IEnumerator Timing()
    {
        while (true)
        {
            if (!_canTimeNow || islosed)
                yield break;

            if (_nowLeftSeconds == 0)
            {
                logicManager.LoseLevel();

                islosed = true;
            }
            else
                _nowLeftSeconds--;
            
            vManager.SetTimeText(MinutesLeft, SecondsWithLeft);

            yield return new WaitForSeconds(1);
        }
    }
}