﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoundController : MonoBehaviour
{
    public static SoundController Instance;

    [SerializeField] private AudioClip clipPlayBtn;
    
    [Space(5)]
    [SerializeField] private AudioClip clipLoseLevel;
    [SerializeField] private AudioClip clipWinLevel;
         
    // Private fields
    private AudioSource _source;

    public static void PlayOneShotSound(AudioClip sound)
    {
        Instance._source.PlayOneShot(sound);
    }

    public void LevelEndSound(bool isWin)
    {
        if (!canLevelSound)
            return;
        
        PlayOneShotSound(isWin ? clipWinLevel : clipLoseLevel);

        StartCoroutine(waitActiveLevelSound());
    }

    private bool canLevelSound = true;
    
    private IEnumerator waitActiveLevelSound()
    {
        canLevelSound = false;
        yield return new WaitForSeconds(1.5f);
        canLevelSound = true;
    }
    
    public void PlayClick()
    {
        PlayOneShotSound(clipPlayBtn);
    }

    private void Awake() 
    {
        // Singleton
        if (Instance == null) 
            Instance = this; 
        else if(Instance == this)
            Destroy(gameObject);

        if (FindObjectsOfType<SoundController>().Length > 1)
            Destroy(gameObject);
            
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        // Installation
        _source = GetComponent<AudioSource>();
            
        // Init event scene load
        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
            
        InitButtonsClick();            
    }

        
    private void SceneManagerOnSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        InitButtonsClick();
    }
    
        
    private void InitButtonsClick()
    {
        foreach (var btn in FindObjectsOfType<Button>())
            btn.onClick.AddListener(PlayClick);
    }
}