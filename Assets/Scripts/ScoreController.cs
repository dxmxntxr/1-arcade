﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    [SerializeField] private VisualManager vManager;

    public int NowScore = 0;

    private bool isPlayedSound = false;
    
    public void AddScore()
    {
        NowScore++;

        if (NowScore > PlayerPrefs.GetInt("Record"))
        {
            PlayerPrefs.SetInt("Record", NowScore);
            
            if (!isPlayedSound)
            {
                SoundController.Instance.LevelEndSound(true);

                isPlayedSound = true;
            }
        }



        vManager.SetNowScore(NowScore);
    }
}
