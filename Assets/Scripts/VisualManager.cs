﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualManager : MonoBehaviour
{
    [SerializeField] private Text nowScoreText;
    [SerializeField] private Text nowTimeText;

    public void SetNowScore(int score)
    {
        nowScoreText.text = "СЧЁТ: " + score;
    }

    public void SetTimeText(int minutes, int seconds)
    {
        var str = seconds > 9 ? "" : "0";
        
        nowTimeText.text = minutes + ":" + str + seconds;
    }

    public void SecondLevelShow()
    {
        nowTimeText.gameObject.SetActive(false);
    }
}
