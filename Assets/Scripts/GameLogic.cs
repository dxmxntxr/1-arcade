﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{
    [SerializeField] private DataSettings settings;

    [Header("Controllers")]
    [SerializeField] private LineUpdater lineUpdater;

    [SerializeField] private CheckController checkController;

    [SerializeField] private ScoreController scoreController;

    [SerializeField] private VisualManager vManager;
    
    [Space(5)]
    [SerializeField] private Button rightBtn;
    [SerializeField] private Button leftBtn;

    [Header("Visuals")]
    [SerializeField] private GameObject windowGameOver;

    private bool isWinLevel = false;
    
    public void LoseLevel()
    {
        windowGameOver.SetActive(true);

        TimeManager.StopTimer();
        
        StartCoroutine(WaitLoadMenu());
        
        SoundController.Instance.LevelEndSound(false);
    }

    public void WinFirstLevel()
    {
        isWinLevel = true;
        
        PlayerPrefs.SetInt("fCompleted", 1);
        
        TimeManager.StopTimer();

        StartCoroutine(WaitLoadMenu());
        
        SoundController.Instance.LevelEndSound(true);
    }
    
    private void Awake()
    {
        if (SoundController.Instance == null)
            SceneManager.LoadScene(0);
        
        windowGameOver.SetActive(false);
        
        switch (settings.NowLevel)
        {
            case 0:
                StartFirstLevel();
                break;
            case 1:
                StartSecondLevel();
                break;
        }
        
        rightBtn.onClick.AddListener(delegate { DoAttack(true); });
        leftBtn.onClick.AddListener(delegate { DoAttack(false); });
    }

    private void DoAttack(bool isRight)
    {
        if (isWinLevel)
            return;
        
        SoundController.Instance.PlayClick();
        
        if (checkController.CheckAttack(isRight))
        {
            lineUpdater.NextLine();
            
            scoreController.AddScore();

            if (settings.NowLevel != 0) return;
            
            if (scoreController.NowScore == 20)
                WinFirstLevel();
        }
        else
        {
            LoseLevel();
        }
    }
    
    private void StartFirstLevel()
    {
        TimeManager.StartTimer(30);
    }

    private void StartSecondLevel()
    {
        vManager.SecondLevelShow();
        
        lineUpdater.StartForeverRun();
    }

    private IEnumerator WaitLoadMenu()
    {
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene(0);
    }
}
