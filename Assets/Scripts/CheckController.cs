﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckController : MonoBehaviour
{
    [SerializeField] private LineUpdater lineUpdater;
    
    public bool CheckAttack(bool isRight)
    {
        var rightName = !isRight ? "right(Clone)" : "left(Clone)";

        return lineUpdater.NowWood.gameObject.name == rightName;
    }
}
