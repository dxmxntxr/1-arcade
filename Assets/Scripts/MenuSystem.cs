﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuSystem : MonoBehaviour
{
    [Header("Buttons")]
    [SerializeField] private Button playBtn;
    [SerializeField] private Button chooseLevelBtn;
    [SerializeField] private Button exitBtn;

    [Space(5)] 
    [SerializeField] private Button btnFirstLevel;
    [SerializeField] private Button btnSecondLevel;

    [SerializeField] private Text recordText;
    
    [Header("Windows")]
    [SerializeField] private GameObject chooseLevelWindow;

    [Header("Data")] [SerializeField] private DataSettings data;

    private void Start()
    {
        playBtn.onClick.AddListener(OnClickPlay);
        chooseLevelBtn.onClick.AddListener(OnClickChooseLevel);
        exitBtn.onClick.AddListener(OnClickExit);

        btnFirstLevel.onClick.AddListener(delegate { OnClickLevelBtn(0); });
        btnSecondLevel.onClick.AddListener(delegate { OnClickLevelBtn(1); });
        
        chooseLevelWindow.SetActive(false);
        
        // Update text record
        recordText.text = "РЕКОРД: " + PlayerPrefs.GetInt("Record");

        btnSecondLevel.interactable = PlayerPrefs.GetInt("fCompleted") == 1;
    }

    private static void OnClickPlay()
    {
        SceneManager.LoadScene(1);
    }

    private void OnClickChooseLevel()
    {
        chooseLevelWindow.SetActive(!chooseLevelWindow.activeSelf);
    }

    private static void OnClickExit()
    {
        Application.Quit();
    }

    private void OnClickLevelBtn(int level)
    {
        data.NowLevel = level;
        
        OnClickPlay();
    }
}
