﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddButtonsSound : MonoBehaviour
{
    [SerializeField] private Button[] buttons;
    
    void Start()
    {
        foreach (var btn in buttons)
        {
            btn.onClick.AddListener(SoundController.Instance.PlayClick);
        }
    }
}
