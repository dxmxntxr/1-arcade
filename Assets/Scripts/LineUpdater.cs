﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineUpdater : MonoBehaviour
{

    [Header("Prefabs")]
    [SerializeField] private GameObject woodLeftPrefab;
    [SerializeField] private GameObject woodRightPrefab;
    
    [Header("Position parameters")]
    [SerializeField] private Vector2 startPosition;
    [SerializeField] private Transform parentWoods;

    [Header("Create parameters")] 
    [SerializeField] private int countCreate;

    [SerializeField] private List<GameObject> currentWoods = new List<GameObject>();

    [Header("Forever parameters")] 
    [SerializeField] private float startWaitTime = 2;

    [SerializeField] private float modiferTime = 0.02f;

    [SerializeField] private float minTime = 0.8f;

    [Space(5)]
    [SerializeField] private GameLogic logic;
    
    // private fields
    private Transform _lastWood;
    public Transform NowWood
    {
        get => currentWoods[0].transform;
    }

    [SerializeField] private float nowWaitTime = -1;

    private void Start()
    {
        CreateStartWoods();
    }

    private void CreateStartWoods()
    {
        startPosition.y -= 1.4f;
        
        if (parentWoods.childCount != 0)
        {
            for (var i = 0; i < parentWoods.childCount; i++)
                Destroy(parentWoods.GetChild(i).gameObject);
        }
        
        for (var i = 0; i < countCreate; i++)
        {
            CreateNewWood();
        }
    }

    private void CreateNewWood()
    {
        // get random wood prefab
        var rand = Random.Range(0, 2);

        var prefabCreate = rand == 0 ? woodLeftPrefab : woodRightPrefab;
        
        // update pos
        var posCreate = _lastWood != null ? (Vector2)_lastWood.localPosition : startPosition;

        posCreate.y += 1.4f;

        var obj = Instantiate(prefabCreate, parentWoods);
        var objTransform = obj.transform;

        objTransform.localPosition = posCreate;
        _lastWood = objTransform;
        
        currentWoods.Add(obj);
    }
    
    public void NextLine()
    {
        Destroy(NowWood.gameObject);
        
        currentWoods.RemoveAt(0);

        foreach (var wood in currentWoods)
        {
            var pos = wood.transform.localPosition;

            pos.y -= 1.4f;

            wood.transform.localPosition = pos;
        }
        
        CreateNewWood();

        if (nowWaitTime == -1) 
            return;
        
        if (nowWaitTime - modiferTime > minTime)
            startWaitTime -= modiferTime;

        nowWaitTime = startWaitTime;
    }

    public void StartForeverRun()
    {
        nowWaitTime = startWaitTime;

        StartCoroutine(EveryForever());
    }

    private bool islosed;
    
    private IEnumerator EveryForever()
    {
        while (true)
        {
            yield return new WaitForSeconds(modiferTime);

            nowWaitTime -= modiferTime;

            if (!(nowWaitTime <= 0) || islosed) continue;

            islosed = true;
            
            logic.LoseLevel();
                
            NextLine();
        }
    }
}
